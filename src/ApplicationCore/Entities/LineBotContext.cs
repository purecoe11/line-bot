﻿using ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace ApplicationCore.Entities
{
	public class LineBotContext : DbContext
	{
		private readonly IApplicationSettings _applicationSettings;
		public LineBotContext(IApplicationSettings applicationSettings)
		{
			_applicationSettings = applicationSettings;
		}
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseMySql(_applicationSettings.GetConnectionString);
		}

		public DbSet<Message> Messages { get; set; }
	}
}