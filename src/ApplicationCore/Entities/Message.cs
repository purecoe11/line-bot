﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApplicationCore.Entities
{
	[Table("message")]
	public class Message
	{
		public int Id { get; set; }
		public string UserId { get; set; }
		public string Description { get; set; }
		public decimal Amount { get; set; }
		public DateTime CreateDate { get; set; }
	}
}