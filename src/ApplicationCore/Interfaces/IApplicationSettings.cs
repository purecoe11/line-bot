﻿namespace ApplicationCore.Interfaces
{
	public interface IApplicationSettings
	{
		string GetAccessToken { get; }
		string GetChannelSecret { get; }
		string GetConnectionString { get; }
	}
}