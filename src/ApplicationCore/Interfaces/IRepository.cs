﻿using System;
using ApplicationCore.Models;
using System.Collections.Generic;

namespace ApplicationCore.Interfaces
{
	public interface IRepository
	{
		void Add(string userId, string description, decimal amount);

		List<UserMessage> GetMessagesByUserId(string id, DateTime startDate, DateTime endDate);
	}
}