﻿namespace ApplicationCore.Models
{
	public class LineEvent
	{
		public string ReplyToken { get; set; }
		public string Type { get; set; }
		public string Timestamp { get; set; }
		public LineSource Source { get; set; }
		public LineMessage Message { get; set; }
	}
}