﻿namespace ApplicationCore.Models
{
	public class LineMessage
	{
		public string Id { get; set; }
		public string Type { get; set; }
		public string Text { get; set; }
	}
}