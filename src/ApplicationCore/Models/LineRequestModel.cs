﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
	public class LineRequestModel
	{
		public string Destination { get; set; }
		public List<LineEvent> Events { get; set; }
	}
}