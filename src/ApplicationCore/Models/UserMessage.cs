﻿using System;

namespace ApplicationCore.Models
{
	public class UserMessage
	{
		public string Description { get; set; }
		public decimal Amount { get; set; }
		public DateTime Time { get; set; }
	}
}