﻿using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Infrastructure.Services;
using Line.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Parties
{
	public class LineBotApp
	{
		private LineMessagingClient LineMessagingClient { get; }
		private readonly IRepository _repository;

		public LineBotApp(IRepository repository,
			IApplicationSettings applicationSettings)
		{
			LineMessagingClient = new LineMessagingClient(applicationSettings.GetAccessToken);
			_repository = repository;
		}

		#region Handlers

		public async Task OnMessageAsync(LineEvent ev)
		{
			switch (ev.Message.Type.ToLower())
			{
				case "text":
					await HandleTextAsync(ev.ReplyToken, ev.Message.Text.Trim(), ev.Source.UserId);
					break;

				default:
					await HandleTextAsync(ev.ReplyToken, "Meoooooow", ev.Source.UserId);
					break;
			}
		}

		#endregion Handlers

		private async Task HandleTextAsync(string replyToken, string userMessage, string userId)
		{
			var messages = new List<ISendMessage>();
			if (!string.IsNullOrEmpty(userMessage))
			{
				var raw = userMessage.Split(' ').ToArray();
				var command = raw[0].ToLower();
				switch (command)
				{
					case "checktoday":
					case "รายการวันนี้":
						messages = SummaryCommandService.Day(_repository, userId);
						break;
					case "checkweek":
					case "รายการสัปดาห์นี้":
						messages = SummaryCommandService.Week(_repository, userId);
						break;
					case "checkmonth":
					case "รายการเดือนนี้":
						messages = SummaryCommandService.Month(_repository, userId);
						break;
					case "help":
						messages = CommandService.GetCommand("en");
						break;
					case "ช่วยเหลือ":
						messages = CommandService.GetCommand("th");
						break;
					case "เมนูอังกฤษ":
						if (!RichMenuService.UpdateRichMenu(LineMessagingClient, userId, "english").Result)
						{
							messages.Add(new TextMessage("Fail Meow"));
						}
						break;
					case "thaimenu":
						if (!RichMenuService.UpdateRichMenu(LineMessagingClient, userId, "thai").Result)
						{
							messages.Add(new TextMessage("Fail Meow"));
						}
						break;
					default:
						messages = AddDataCommandService.Process(_repository, userId, raw);
						break;
				}
			}

			await SendLineResponse(replyToken, userId, messages);
		}

		private async Task SendLineResponse(string replyToken, string userId, List<ISendMessage> messages)
		{
			try
			{
				for (var i = 0; i < (double)messages.Count / 5; i++)
				{
					if (i == 0)
						await LineMessagingClient.ReplyMessageAsync(replyToken, messages.Take(5).ToList());
					else
						await LineMessagingClient.PushMessageAsync(userId, messages.Skip(i * 5).Take(5).ToList());
				}
			}
			catch (LineResponseException ex)
			{
				if (ex.Message == "Invalid reply token")
				{
					try
					{
						for (var i = 0; i < (double)messages.Count / 5; i++)
						{
							await LineMessagingClient.PushMessageAsync(userId, messages.Skip(i * 5).Take(5).ToList());
						}
					}
					catch (LineResponseException innerEx)
					{
						await LineMessagingClient.PushMessageAsync(userId, innerEx.Message);
					}
				}
				else
				{
					await LineMessagingClient.PushMessageAsync(userId, ex.Message);
				}
			}
			catch (Exception ex)
			{
				await LineMessagingClient.PushMessageAsync(userId, ex.Message);
			}
		}
	}
}