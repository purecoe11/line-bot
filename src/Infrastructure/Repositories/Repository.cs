﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using TimeZoneConverter;

namespace Infrastructure.Repositories
{
	public class Repository : IRepository
	{
		private readonly LineBotContext _lineBotContext;

		public Repository(LineBotContext lineBotContext)
		{
			_lineBotContext = lineBotContext;
		}

		public void Add(string userId, string description, decimal amount)
		{
			_lineBotContext.Messages.Add(new Message
			{
				UserId = userId,
				Amount = amount,
				Description = description,
				CreateDate = DateTime.UtcNow
			});
			_lineBotContext.SaveChanges();
		}

		public List<UserMessage> GetMessagesByUserId(string id, DateTime startDate, DateTime endDate)
		{
			startDate = startDate.ToUniversalTime();
			endDate = endDate.ToUniversalTime();
			var zone = TZConvert.WindowsToIana("SE Asia Standard Time");
			var thZone = TimeZoneInfo.FindSystemTimeZoneById(zone);
			return _lineBotContext.Messages.Where(x =>
					x.UserId.Equals(id) &&
					x.CreateDate >= startDate &&
					x.CreateDate < endDate)
					.Select(x => new UserMessage
					{
						Description = x.Description,
						Amount = x.Amount,
						Time = TimeZoneInfo.ConvertTimeFromUtc(x.CreateDate, thZone)
					})
				.ToList();
		}
	}
}