﻿using ApplicationCore.Interfaces;
using Line.Messaging;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Infrastructure.Services
{
	public static class AddDataCommandService
	{
		public static List<ISendMessage> Process(IRepository repository, string userId, string[] raw)
		{
			var regex = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
			var messages = new List<ISendMessage>();
			if (raw.Length > 1)
			{
				var lastIndex = raw.Length - 1;
				var description = string.Empty;
				if (!regex.IsMatch(raw[lastIndex]) || !decimal.TryParse(raw[lastIndex], out var amount)) return messages;
				for (var i = 0; i < lastIndex; i++)
				{
					description = $"{description} {raw[i]}";
				}
				repository.Add(userId, description, amount);
				messages.Add(new TextMessage("OK Meow"));
			}
			else
			{
				messages.Add(new TextMessage("I don't understand Meow"));
			}

			return messages;
		}
	}
}