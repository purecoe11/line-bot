﻿using Line.Messaging;
using System.Collections.Generic;

namespace Infrastructure.Services
{
	public static class CommandService
	{
		public static List<ISendMessage> GetCommand(string language)
		{
			const string commandsEnglish = "How to add new data\n" +
			                               " - text message that you want to add and add the amount at the end of text.\n" +
			                               "\nExample\n" +
			                               "2 noodles 40";
			const string commandsThai = "วิธีการเพิ่มข้อมูล\n" +
			                            " - พิมพ์รายการที่ต้องการจัดเก็บและใส่จำนวนเงินท้ายสุดของข้อความ" +
			                            "\nยกตัวอย่างเช่น\n" +
			                            "ก๋วยเตี๋ยว 2 ชาม 40";
			var messages = new List<ISendMessage> { new TextMessage(language.Equals("en") ? commandsEnglish : commandsThai) };
			return messages;
		}
	}
}