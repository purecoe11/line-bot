﻿using Line.Messaging;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
	public static class RichMenuService
	{
		public static async Task<bool> UpdateRichMenu(LineMessagingClient lineMessagingClient, string userId, string key)
		{
			var richMenus = lineMessagingClient.GetRichMenuListAsync().Result;
			var richMenu = richMenus.FirstOrDefault(x => x.Name.ToLower().Contains(key));
			if (richMenu == null)
			{
				return false;
			}
			await lineMessagingClient.LinkRichMenuToUserAsync(userId, richMenu.RichMenuId);
			return true;
		}
	}
}