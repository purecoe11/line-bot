﻿using Line.Messaging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Infrastructure.Services
{
	public static class SummaryCommandService
	{
		public static List<ISendMessage> Day(ApplicationCore.Interfaces.IRepository repository, string userId)
		{
			var start = DateTime.Today;
			var end = start.AddDays(1);
			var data = repository.GetMessagesByUserId(userId, start, end);
			var messages = new List<ISendMessage>();
			if (data.Count > 0)
			{
				var msg = new StringBuilder();
				for (var i = 0; i < data.Count; i++)
				{
					if (i % 5 == 0 && i > 0)
					{
						messages.Add(new TextMessage(msg.ToString()));
						msg.Clear();
					}
					else
					{
						msg.Append(i > 0 ? "\n\n" : "");
					}
					msg.Append($"{data[i].Time.ToString("HH:mm:ss", CultureInfo.InvariantCulture)}\n" +
							   $"{data[i].Description}\n" +
							   $"{data[i].Amount:##,###} บาท");
				}
				var replyMessage = msg.ToString();
				if (!string.IsNullOrEmpty(replyMessage))
				{
					messages.Add(new TextMessage(replyMessage));
				}
				messages.Add(new TextMessage($"สรุป => {data.Sum(x => x.Amount):##,###} บาท Meow"));
			}
			else
			{
				messages.Add(new TextMessage("ไม่มีรายการ Meow~"));
			}
			return messages;
		}

		public static List<ISendMessage> Week(ApplicationCore.Interfaces.IRepository repository, string userId)
		{
			var start = GetFirstOfWeek();
			var end = DateTime.Today.AddDays(1);
			var data = repository.GetMessagesByUserId(userId, start, end);
			var messages = new List<ISendMessage>();
			if (data.Count > 0)
			{
				foreach (var itemGroup in data.GroupBy(x => x.Time.Date))
				{
					var msg = new StringBuilder();
					msg.Append($"{itemGroup.Key.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)}\n");
					foreach (var item in itemGroup)
					{
						msg.Append($"{item.Description}: {item.Amount:##,###} บาท\n");
					}
					msg.Append($"สรุป => {itemGroup.Sum(x => x.Amount):##,###} บาท");
					messages.Add(new TextMessage(msg.ToString()));
				}
				messages.Add(new TextMessage($"สรุป => {data.Sum(x => x.Amount):##,###} บาท Meow"));
			}
			else
			{
				messages.Add(new TextMessage("ไม่มีรายการ Meow~"));
			}
			return messages;
		}

		public static List<ISendMessage> Month(ApplicationCore.Interfaces.IRepository repository, string userId)
		{
			var start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
			var end = start.AddMonths(1);
			var data = repository.GetMessagesByUserId(userId, start, end);
			var messages = new List<ISendMessage>();
			if (data.Count > 0)
			{
				var msg = new StringBuilder();
				foreach (var itemGroup in data.GroupBy(x => x.Time.Date))
				{
					msg.Append($"{itemGroup.Key.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)} => {itemGroup.Sum(x => x.Amount):##,###} บาท\n");
				}
				var allMsg = msg.ToString();
				messages.Add(new TextMessage(allMsg.Substring(0, allMsg.Length - 2)));
				messages.Add(new TextMessage($"สรุป => {data.Sum(x => x.Amount):##,###} บาท Meow"));
			}
			else
			{
				messages.Add(new TextMessage("ไม่มีรายการ Meow~"));
			}
			return messages;
		}

		private static DateTime GetFirstOfWeek()
		{
			return CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek == DateTime.Today.DayOfWeek
				? DateTime.Today.AddDays(-6)
				: DateTime.Today.AddDays(-1 * (int)DateTime.Today.DayOfWeek + 1);
		}
	}
}