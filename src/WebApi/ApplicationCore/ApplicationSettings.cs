﻿using ApplicationCore.Interfaces;
using Microsoft.Extensions.Configuration;

namespace WebApi.ApplicationCore
{
	public class ApplicationSettings : IApplicationSettings
	{
		private readonly IConfiguration _configuration;

		public ApplicationSettings(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public string GetAccessToken => _configuration.GetValue<string>("AccessToken");
		public string GetChannelSecret => _configuration.GetValue<string>("ChannelSecret");
		public string GetConnectionString => _configuration.GetValue<string>("LineConnectionString");
	}
}