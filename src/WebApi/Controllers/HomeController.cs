﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using WebApi.Extensions;

namespace WebApi.Controllers
{
	public class HomeController : ApiControllerBase
	{
		private readonly string _environmentName;

		public HomeController(IHostingEnvironment environment)
		{
			_environmentName = environment.EnvironmentName
				.ToLower()
				.Equals("production")
				? string.Empty
				: $" ({environment.EnvironmentName})";
		}

		[HttpGet]
		public ObjectResult Index()
		{
			return Ok(new { Greetings = $"Welcome To Line Bot Api{_environmentName}." });
		}
	}
}