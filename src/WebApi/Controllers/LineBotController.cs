﻿using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Infrastructure.Parties;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebApi.Extensions;
using WebApi.Helpers;

namespace WebApi.Controllers
{
	public class LineBotController : ApiControllerBase
	{
		private readonly IApplicationSettings _applicationSettings;
		private readonly LineBotApp _lineBotApp;
		private readonly IHttpContextAccessor _httpContextAccessor;

		public LineBotController(IApplicationSettings applicationSettings,
			LineBotApp lineBotApp,
			IHttpContextAccessor httpContextAccessor)
		{
			_applicationSettings = applicationSettings;
			_lineBotApp = lineBotApp;
			_httpContextAccessor = httpContextAccessor;
		}

		[HttpPost]
		public async Task<IActionResult> TaskNoSecure([FromBody] JObject json)
		{
			var content = string.Empty;
			
			try
			{
				if (_httpContextAccessor.HttpContext.Request.Body.CanSeek)
				{
					_httpContextAccessor.HttpContext.Request.Body.Seek(0, SeekOrigin.Begin);
					var sr = new StreamReader(_httpContextAccessor.HttpContext.Request.Body);
					content = sr.ReadToEnd();
				}
				var model = JsonConvert.DeserializeObject<LineRequestModel>(content);
				await _lineBotApp.OnMessageAsync(model.Events.First());
				return Ok();
			}
			catch (Exception e)
			{
				throw new Exception($"Error {content}{Environment.NewLine}{Environment.NewLine}{e}");
			}
		}

		[HttpPost]
		public async Task<IActionResult> Task([FromBody] JObject json)
		{
			var content = string.Empty;
			try
			{
				if (_httpContextAccessor.HttpContext.Request.Body.CanSeek)
				{
					_httpContextAccessor.HttpContext.Request.Body.Seek(0, SeekOrigin.Begin);
					var sr = new StreamReader(_httpContextAccessor.HttpContext.Request.Body);
					content = sr.ReadToEnd();
				}
				var model = HttpContext.Request.GetWebhookEvents(_applicationSettings.GetChannelSecret, content);
				await _lineBotApp.OnMessageAsync(model.Events.First());
				return Ok();
			}
			catch (Exception e)
			{
				throw new Exception($"Error {content}{Environment.NewLine}{Environment.NewLine}{e}");
			}
		}

		[HttpPost]
		public IActionResult Test([FromBody] JObject request)
		{
			return Ok();
		}

		[HttpPost]
		public IActionResult Check()
		{
			return Ok(new { Secret = _applicationSettings.GetChannelSecret, Token = _applicationSettings.GetAccessToken });
		}
	}
}