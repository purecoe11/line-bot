﻿using Microsoft.AspNetCore.Mvc;

namespace WebApi.Extensions
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class ApiControllerBase : ControllerBase
	{
	}
}