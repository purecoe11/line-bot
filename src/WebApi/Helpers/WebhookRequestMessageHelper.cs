﻿using ApplicationCore.Models;
using Line.Messaging;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace WebApi.Helpers
{
	public static class WebhookRequestMessageHelper
	{
		public static LineRequestModel GetWebhookEvents(this HttpRequest request, string channelSecret, string content)
		{
			if (request == null) { throw new ArgumentNullException(nameof(request)); }
			if (channelSecret == null) { throw new ArgumentNullException(nameof(channelSecret)); }

			var xLineSignature = request.Headers["X-Line-Signature"].FirstOrDefault();
			if (string.IsNullOrEmpty(xLineSignature) || !VerifySignature(channelSecret, xLineSignature, content))
			{
				var error = $"{GetHashString(channelSecret, content)} : {xLineSignature}";
				throw new InvalidSignatureException(error);
			}

			return JsonConvert.DeserializeObject<LineRequestModel>(content);
		}

		private static bool VerifySignature(string channelSecret, string xLineSignature, string requestBody)
		{
			try
			{
				var key = Encoding.UTF8.GetBytes(channelSecret);
				var body = Encoding.UTF8.GetBytes(requestBody);

				using (var hmac = new HMACSHA256(key))
				{
					var hash = hmac.ComputeHash(body, 0, body.Length);
					var xLineBytes = Convert.FromBase64String(xLineSignature);
					return SlowEquals(xLineBytes, hash);
				}
			}
			catch
			{
				return false;
			}
		}

		private static string GetHashString(string channelSecret, string requestBody)
		{
			try
			{
				var key = Encoding.UTF8.GetBytes(channelSecret);
				var body = Encoding.UTF8.GetBytes(requestBody);

				using (var hmac = new HMACSHA256(key))
				{
					var hash = hmac.ComputeHash(body, 0, body.Length);
					return Convert.ToBase64String(hash);
				}
			}
			catch
			{
				return "fail";
			}
		}

		private static bool SlowEquals(byte[] a, byte[] b)
		{
			uint diff = (uint)a.Length ^ (uint)b.Length;
			for (int i = 0; i < a.Length && i < b.Length; i++)
				diff |= (uint)(a[i] ^ b[i]);
			return diff == 0;
		}
	}
}